import { Hero } from '../Hero/Hero';

export class UnevenHero {
  protected hero: Hero;

  constructor(hero: Hero) {
    this.hero = hero;
  }
  getHero(): Hero {
    return this.hero;
  }
}
