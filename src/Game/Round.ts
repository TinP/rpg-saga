import { Hero } from '../Hero/Hero';

import { Pair } from './Pair';
import { Duel } from './Duel';

export class Round {
  roundWinners = [];
  unevenHero: Hero = null;
  arrayOfHeroesTest = new Array<Hero>();
  startRound(arrayOfHeroes: Hero[]) {
    console.log('-------------------------- Round Start --------------------------');

    if (arrayOfHeroes.length % 2 !== 0) {
      console.log('There is an uneven number of heroes in this round.');
      this.unevenHero = arrayOfHeroes.pop();
      console.log(`\n\nThe uneven hero is: ${this.unevenHero.getHeroFirstAndLastName()}`);
      console.log('\nAfter removing theh uneven hero the array of heroes is: ');
      arrayOfHeroes.forEach(hero => console.log(hero.getHeroFirstAndLastName()));
      this.roundWinners.push(this.unevenHero);
      this.startRound(arrayOfHeroes);
    }
    console.log('There is an even number of heroes in this round.');
    while (arrayOfHeroes.length > 1) {
      const pair = this.makePairs(arrayOfHeroes);
      const newDuel = new Duel();
      newDuel.startDuel(pair);
      console.log(`\n\n`);
      console.log(newDuel.winner.getHeroFirstAndLastName());
      this.roundWinners.push(newDuel.winner);
    }
    console.log(`\n\n\n-------------------------- End Of Round--------------------------`);
    if (this.roundWinners.length > 1) {
      console.log('Round finished. Winners are: ');
      this.roundWinners.forEach(winner => console.log(winner.getHeroFirstAndLastName()));
      console.log(`\n\n\n-------------------------- Next Round --------------------------`);
      return this.startRound(this.roundWinners);
    } else {
      console.log(`\n\n\n-------------------------- End Of Game --------------------------`);
      return this.victory(this.roundWinners[0]);
    }
  }

  makePairs(arrayOfHeroes: Hero[]) {
    let hero2: Hero[];
    const hero1index = this.rollIndex(arrayOfHeroes.length);
    const hero1: Hero[] = arrayOfHeroes.splice(hero1index, 1);
    console.log(`Hero 1 index: ${hero1index}`);
    const hero2index = this.rollIndex(arrayOfHeroes.length);
    console.log(`Hero 2 index: ${hero2index}`);

    // eslint-disable-next-line prefer-const
    hero2 = arrayOfHeroes.splice(hero2index, 1);
    const newPair = new Pair(hero1, hero2);
    // console.log(newPair.first);
    // console.log(newPair.second);
    return newPair;
  }

  rollIndex(num: number) {
    return Math.floor(Math.random() * num);
  }

  victory(hero: Hero) {
    console.log(`The winner of the game is: ${hero.getHeroFirstAndLastName()}`);
  }
}

function rollNum(int) {
  return Math.floor(Math.random() * int);
}
