import { HeroGen } from '../HeroGen/HeroGen';

import { Round } from './Round';
// import { Duel } from './Duel';

// const prompt = require('prompt-sync')({ sigint: true });

export class Game {
  numOfHeroes = 4;
  peasantCount = 0;
  newRound: Round;
  arrayOfHeroes = [];

  run() {
    console.log('Starting game');
    console.log(`Number of Heroes: `);
    // this.numOfHeroes = Number(prompt());
    this.populate(this.numOfHeroes);
    const round = new Round();
    round.startRound(this.arrayOfHeroes);
  }

  private populate(numOfHeroes: number) {
    for (let i = 0; i < numOfHeroes; i++) {
      const hero = new HeroGen();
      hero.createHero(this.peasantCount);
      if (hero.hero.getHeroClass() === 'Peasant') {
        this.peasantCount++;
        console.log('Peasant created');
      }
      this.arrayOfHeroes.push(hero.hero);
    }
    // this.arrayOfHeroes.forEach(hero => console.log(hero));
    this.arrayOfHeroes.forEach(hero =>
      //   console.log(hero.getHeroFirstAndLastName(), " ", hero.getHeroClassName(), " HP:", hero.getCurrentHealthPoints(), " | Armor Points: ",hero.getDefensePoints(), " |  Attack Points: ",hero.getAttackPoints()),
      console.log(hero.getHeroFirstAndLastName(), '-', hero.getHeroClass(), ' HP:', hero.getCurrentHealthPoints()),
    );
  }

  gameOver() {
    console.log('Game over.');
  }
}
