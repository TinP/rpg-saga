import { Ability } from '../HeroGen/Ability';
import { Hero } from '../Hero/Hero';

import { Pair } from './Pair';

export class Duel {
  winner: Hero = null;
  turnCount = 0;
  startDuel(pair: Pair) {
    // console.log(pair);
    // console.log("First: ", pair.first);
    // console.log("Second: ", pair.second);
    let player1: Hero;
    let player2: Hero;

    switch (Math.floor(this.flipCoin())) {
      case 0:
        player1 = pair.first[0];
        player2 = pair.second[0];
        break;
      case 1:
        player1 = pair.second[0];
        player2 = pair.first[0];
        break;
    }
    this.checkPeasant(player1, player2);
    if (this.winner !== null) {
      return this.winner;
    }
    console.log(`Player1: ${player1.getHeroFirstAndLastName()} the ${player1.getHeroClass()}`);
    console.log(`Player2: ${player2.getHeroFirstAndLastName()} the ${player2.getHeroClass()}`);
    if (this.winner !== null) {
      return this.winner;
    }
    this.startTurn(this.turnCount, player1, player2);
  }

  checkPeasant(player1: Hero, player2: Hero) {
    const player1Class = player1.getHeroClass();
    const player2Class = player2.getHeroClass();

    if (player1Class === 'Peasant') {
      this.duelOver(player2, player1);
    } else if (player2Class === 'Peasant') {
      this.duelOver(player1, player2);
    }
  }

  startTurn(turnCount: number, hero1: Hero, hero2: Hero) {
    turnCount = turnCount + 1; // eslint-disable-line no-param-reassign
    console.log(`Turn Count: ${turnCount}`);
    console.log(hero1.getHeroFirstAndLastName(), ' remaining HP: ', hero1.getCurrentHealthPoints());
    console.log(hero2.getHeroFirstAndLastName(), ' remaining HP: ', hero2.getCurrentHealthPoints(), '\n\n');
    this.turnAction(hero1, hero2);
    this.turnAction(hero2, hero1);
    this.checkHealth(turnCount, hero1, hero2);
  }

  turnAction(attacker: Hero, defender: Hero) {
    if (attacker.getAbilityPoints() !== 0) {
      this.useAbility(attacker, defender);
    }
    this.basicAttack(attacker, defender);
  }

  basicAttack(attacker: Hero, defender: Hero) {
    const attackerAP = attacker.getAttackPoints();
    const defenderDP = defender.getDefensePoints();
    const damage = attackerAP;

    if (damage < 0) {
      console.log(attacker.getFirstName(), ' deals 0 damage');
      return;
    } else {
      defender.takeDamage(damage);
      console.log(
        `${attacker.getHeroFirstAndLastName()} hit ${defender.getHeroFirstAndLastName()} dealing `,
        damage,
        ' points of damage.',
      );
    }
  }

  checkHealth(turnCount: number, hero1: Hero, hero2: Hero) {
    if (hero1.getCurrentHealthPoints() <= 0 && hero2.getCurrentHealthPoints() > hero1.getCurrentHealthPoints()) {
      this.duelOver(hero2, hero1);
    } else if (hero2.getCurrentHealthPoints() <= 0 && hero1.getCurrentHealthPoints() > hero2.getCurrentHealthPoints()) {
      this.duelOver(hero1, hero2);
    } else {
      this.startTurn(turnCount, hero1, hero2);
    }
  }

  flipCoin() {
    return Math.floor(Math.random() * 2);
  }

  duelOver(winner: Hero, loser: Hero) {
    winner.resetAttackPoints();
    winner.resetArmorPoints();
    winner.resetHealthPoints();
    console.log(
      'Winner of the duel between ',
      loser.getHeroFirstAndLastName(),
      ' and ',
      winner.getHeroFirstAndLastName(),
      ' is...',
    );
    console.log(`${winner.getHeroFirstAndLastName()} the ${winner.getHeroClass()}.`);
    this.winner = winner;
  }
  peasantDuel() {
    console.log('Both fighters were Peasants, no winners.');
    return 0;
  }

  checkAbility(hero: Hero, opponent: Hero) {
    if (hero.getAbilityPoints() > 0) {
      this.useAbility(hero, opponent);
    } else {
      console.log('Hero has no abilityPoints left.');
    }
  }
  useAbility(hero: Hero, opponent: Hero) {
    const ability: Ability = hero.getSpecialAbility();
    switch (ability.getAbilityName()) {
      case 'Fire Arrow':
        hero.setAbilityPoints(hero.getAbilityPoints() - 1);
        console.log(`${hero.getHeroFirstAndLastName()} used ${hero.getSpecialAbility().getAbilityName()}`);
        hero.setAttackPoints(hero.getAttackPoints() + 3);
        break;
      case 'Nemesis':
        hero.setAbilityPoints(hero.getAbilityPoints() - 1);
        console.log(`${hero.getHeroFirstAndLastName()} used ${hero.getSpecialAbility().getAbilityName()}`);
        hero.setAttackPoints(Math.ceil(hero.getAttackPoints() * 1.3));
        break;
      case 'Slash':
        hero.setAbilityPoints(hero.getAbilityPoints() - 1);
        console.log(`${hero.getHeroFirstAndLastName()} used ${hero.getSpecialAbility().getAbilityName()}`);
        hero.setAttackPoints(Math.ceil(hero.getAttackPoints() * 1.5));
        break;
      case 'Surrender':
        hero.setHealthPoints(0);
        console.log(`${hero.getHeroFirstAndLastName()} surrendered.`);
        this.checkHealth(this.turnCount, hero, opponent);
        break;
    }
  }
}
