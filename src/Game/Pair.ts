import { Hero } from '../Hero/Hero';

export class Pair {
  first: Hero[];
  second: Hero[];

  constructor(first: Hero[], second: Hero[]) {
    this.first = first;
    this.second = second;
  }
  returnFirst() {
    return this.first[0];
  }
  returnSecond() {
    return this.second[0];
  }
}
