export class Ability {
  public name: string;
  public numTicks: number;

  constructor(name: string, numTicks: number) {
    this.name = name;
    this.numTicks = numTicks;
  }

  public getAbilityName(): string {
    return this.name;
  }

  public getAbilitynumTicks(): number {
    return this.numTicks;
  }
}
