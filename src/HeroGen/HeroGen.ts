import { Archer } from '../Hero/Archer';
import { Knight } from '../Hero/Knight';
import { Peasant } from '../Hero/Peasant';
import { Wizard } from '../Hero/Wizard';
import { Rogue } from '../Hero/Rogue';
import { Hero } from '../Hero/Hero';

import { Ability } from './Ability';

let maxNum = 5;
export class HeroGen {
  hero: Hero;
  createHero(peasantCount: number) {
    if (peasantCount > 1) {
      maxNum = 4;
    }
    const heroClassIndex = this.numRoll(maxNum) + 1;
    switch (heroClassIndex) {
      case 1:
        this.hero = this.createKnight();
        break;
      case 2:
        this.hero = this.createRogue();
        break;
      case 3:
        this.hero = this.createWizard();
        break;
      case 4:
        this.hero = this.createArcher();
        break;
      case 5:
        this.hero = this.createPeasant();
    }
    return this.hero[1];
  }
  numRoll(int: number) {
    return Math.floor(Math.random() * int);
  }
  randomHeroName() {
    const names = ['John', 'Joseph', 'Jeremiah', 'James', 'Jake', 'Johnathan', 'Josh'];
    return names[this.numRoll(names.length)];
  }
  randomHeroLastName() {
    const lastNames = ['Joshson', 'Westson', 'Schnell', 'Brown', 'Grump', 'Vercetti', 'Newerson'];
    return lastNames[this.numRoll(lastNames.length)];
  }

  randomAttackPoints() {
    return this.numRoll(6) + 3;
  }

  randomHealthPoints() {
    return this.numRoll(25) + 19;
  }
  randomDefensePoints() {
    return this.numRoll(3) + 3;
  }

  createKnight(): Hero {
    return new Knight(
      this.randomHeroName(),
      this.randomHeroLastName(),
      'Knight',
      this.randomHealthPoints() + 1,
      this.randomAttackPoints() - 1,
      this.randomDefensePoints() - 1,
      1,
      new Ability('Nemesis', 3),
    );
  }
  createWizard(): Hero {
    return new Wizard(
      this.randomHeroName(),
      this.randomHeroLastName(),
      'Wizard',
      this.randomHealthPoints() - 2,
      this.randomAttackPoints() + 2,
      0,
      1,
      new Ability('Bewitched', 2),
    );
  }

  createArcher(): Hero {
    return new Archer(
      this.randomHeroName(),
      this.randomHeroLastName(),
      'Archer',
      this.randomHealthPoints() + 1,
      this.randomAttackPoints() + 2,
      0,
      1,
      new Ability('FireArrow', 3),
    );
  }
  createRogue(): Hero {
    return new Rogue(
      this.randomHeroName(),
      this.randomHeroLastName(),
      'Rogue',
      this.randomHealthPoints() - 1,
      this.randomAttackPoints() + 2,
      0,
      1,
      new Ability('Slash', 1),
    );
  }
  createPeasant(): Hero {
    return new Peasant(
      this.randomHeroName(),
      this.randomHeroLastName(),
      'Peasant',
      1,
      0,
      0,
      1,
      new Ability('Surrender', 1),
    );
  }
}
