import { Game } from './Game/Game';

function main() {
  const newGame = new Game();
  newGame.run();
}

main();
