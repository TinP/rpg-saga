import { Ability } from '../HeroGen/Ability';
import { HeroGen } from '../HeroGen/HeroGen';

export abstract class Hero {
  protected firstName: string;
  protected lastName: string;
  protected heroClass: string;
  protected specialAbility: Ability;
  protected currentHealth: number;
  protected healthPoints: number;
  protected attackPoints: number;
  protected currentAttackPoints: number;
  protected defensePoints: number;
  protected abilityPoints: number;

  constructor(
    firstName: string,
    lastName: string,
    heroClass: string,
    healthPoints: number,
    attackPoints: number,
    defensePoints: number,
    abilityPoints: number,
    specialAbility: Ability,
  ) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.heroClass = heroClass;
    this.specialAbility = specialAbility;
    this.healthPoints = healthPoints;
    this.currentHealth = healthPoints;
    this.attackPoints = attackPoints;
    this.currentAttackPoints = attackPoints;
    this.defensePoints = defensePoints;
    this.abilityPoints = abilityPoints;
  }

  newHero(peasantCount: number) {
    const heroGen = new HeroGen();
    let tempHero = Hero;
    tempHero = heroGen.createHero(peasantCount);
    return tempHero;
  }
  // getters
  getFirstName() {
    return this.firstName;
  }
  getLastName() {
    return this.lastName;
  }

  getHeroFirstAndLastName() {
    return `${this.firstName} ${this.lastName}.`;
  }

  getHeroClass() {
    return this.heroClass;
  }
  getSpecialAbility() {
    return this.specialAbility;
  }
  getAttackPoints() {
    return this.attackPoints;
  }
  getCurrentHealthPoints() {
    return this.currentHealth;
  }
  getDefensePoints() {
    return this.defensePoints;
  }
  getAbilityPoints() {
    return this.abilityPoints;
  }

  // setters
  setAttackPoints(attackPoints: number) {
    this.attackPoints = attackPoints;
  }
  setHealthPoints(healthPoints: number) {
    this.currentHealth = healthPoints;
  }
  setDefensePoints(defensePoints: number) {
    this.defensePoints = defensePoints;
  }
  setAbilityPoints(abilityPoints: number) {
    this.abilityPoints = abilityPoints;
  }

  // methods
  resetHealthPoints() {
    this.currentHealth = this.healthPoints;
  }
  resetAttackPoints() {
    this.currentAttackPoints = this.attackPoints;
  }
  resetArmorPoints() {
    this.currentHealth = this.healthPoints;
  }
  takeDamage(enemyAttack: number) {
    this.currentHealth -= enemyAttack;
  }
}
