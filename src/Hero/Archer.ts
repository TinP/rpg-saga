import { Ability } from '../HeroGen/Ability';

import { Hero } from './Hero';

export class Archer extends Hero {
  constructor(
    firstName: string,
    lastName: string,
    heroClass: string,
    healthPoints: number,
    attackPoints: number,
    defensePoints: number,
    abilityPoints: number,
    specialAbility: Ability,
  ) {
    super(firstName, lastName, heroClass, healthPoints, attackPoints, defensePoints, abilityPoints, specialAbility);
  }
}
